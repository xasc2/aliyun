data "alicloud_kvstore_zones" "zones_ids" {
  multi = true
}

module "vswitch_prp_zf" {
  source      = "./modules/vswitch"

  vswitch_availability_zone = "cn-shanghai-f"
  vswitch_vpc_id      = local.prp_vpc_id
  vswitch_cidr_block = local.prp_vsw_zf_CIDR
  vswitch_name = "lv-vsw-prp-csl-zf"
  vswitch_tag_project = "CSL"
  vswitch_tag_env = "Preproduction"
}

module "vswitch_prp_zg" {
  source      = "./modules/vswitch"

  vswitch_availability_zone = "cn-shanghai-g"
  vswitch_vpc_id      = local.prp_vpc_id
  vswitch_cidr_block = local.prp_vsw_zg_CIDR
  vswitch_name = "lv-vsw-prp-csl-zg"
  vswitch_tag_project = "CSL"
  vswitch_tag_env = "Preproduction"
}

module "vswitch_prd_zf" {
  source      = "./modules/vswitch"

  vswitch_availability_zone = "cn-shanghai-f"
  vswitch_vpc_id      = local.prd_vpc_id
  vswitch_cidr_block = local.prd_vsw_zf_CIDR
  vswitch_name = "lv-vsw-prd-csl-zf"
  vswitch_tag_project = "CSL"
  vswitch_tag_env = "Production"
}

module "vswitch_prd_zg" {
  source      = "./modules/vswitch"

  vswitch_availability_zone = "cn-shanghai-g"
  vswitch_vpc_id      = local.prd_vpc_id
  vswitch_cidr_block = local.prd_vsw_zg_CIDR
  vswitch_name = "lv-vsw-prd-csl-zg"
  vswitch_tag_project = "CSL"
  vswitch_tag_env = "Production"
}


# Create Security Groups
resource "alicloud_security_group" "prd_nsg" {
  count       =  length(local.prd_nsg_names)
  name        =  local.prd_nsg_names[count.index]
  description =  local.prd_nsg_names[count.index]
  security_group_type = "enterprise"
  vpc_id = local.prd_vpc_id
  resource_group_id = "rg-aek2lmtbti4wjxa"
}


resource "alicloud_security_group" "prp_nsg" {
  count       =  length(local.prp_nsg_names)
  name        =  local.prp_nsg_names[count.index]
  description =  local.prp_nsg_names[count.index]
  security_group_type ="enterprise"
  vpc_id = local.prp_vpc_id
  resource_group_id = "rg-aek2hxhh3x7xsoa"
}

# ========== prp kms, ecs, mysql, oss===========
module "kms_key_prp" {
  source = "./modules/kms"
  
  kms_alias_name = "alias/lv-kms-prp-csl"
}

module "prp_ecs01" {
  source = "./modules/ecs"
  
  data_alicloud_images_name_regex     = local.local_image_name_regex
  instance_type                       = local.prp_local_instance_type
  security_groups                     = [alicloud_security_group.prp_nsg.0.id]
  availability_zone                   = local.local_availability_zone
  instance_name                       = "lvcnlnx6056q"
  system_disk_category                = "cloud_essd"
  system_disk_auto_snapshot_policy_id = "sp-uf69vpvofpmmj89st4ds"
  host_name                           = "lvcnlnx6056q"
  vswitch_id                          = module.vswitch_prp_zg.vswitch_id
  resource_group_id                   = "rg-aek2hxhh3x7xsoa"
  tags_lbnref                         = "73152"
  tags_brand                          = "LVM"
  tags_application                    = "FE"
  tags_environment                    = "Preproduction"
  tags_project                        = "CSL"
}

module "prp_ecs02" {
  source = "./modules/ecs"

  data_alicloud_images_name_regex     = local.local_image_name_regex
  instance_type                       = local.prp_local_instance_type
  security_groups                     = [alicloud_security_group.prp_nsg.1.id]
  availability_zone                   = local.local_availability_zone
  instance_name                       = "lvcnlnx6057q"
  system_disk_category                = "cloud_essd"
  system_disk_auto_snapshot_policy_id = "sp-uf69vpvofpmmj89st4ds"
  host_name                           = "lvcnlnx6057q"
  vswitch_id                          = module.vswitch_prp_zg.vswitch_id
  resource_group_id                   = "rg-aek2hxhh3x7xsoa"
  tags_lbnref                         = "73153"
  tags_brand                          = "LVM"
  tags_application                    = "BO"
  tags_environment                    = "Preproduction"
  tags_project                        = "CSL"
}

module "prp_mysql01" {
  source = "./modules/mysql"

  db_instance_type         = local.prp_local_mysql_type
  db_instance_storage_type = "cloud_ssd"
  db_engine_version        = "8.0"
  db_instance_storage      = 20
  db_instance_name         = "lv-mysql-prp-csl"
  db_resource_group_id     = "rg-aek2hxhh3x7xsoa"
  db_zone_id               = local.local_availability_zone
  db_vswitch_id            = module.vswitch_prp_zg.vswitch_id
  db_security_group_ids    = alicloud_security_group.prp_nsg.*.id
  db_tag_project           = "CSL"
  db_tag_environment       = "Preproduction"
}

# ========== prd kms, ecs, mysql===========
module "kms_key_prd" {
  source = "./modules/kms"
  
  kms_alias_name = "alias/lv-kms-prd-csl"
}

module "prd_ecs01" {
  source = "./modules/ecs"
  
  data_alicloud_images_name_regex     = local.local_image_name_regex
  instance_type                       = local.prd_local_instance_type
  security_groups                     = [alicloud_security_group.prd_nsg.0.id]
  availability_zone                   = local.local_availability_zone
  instance_name                       = "lvcnlnx6058p"
  system_disk_category                = "cloud_essd"
  system_disk_auto_snapshot_policy_id = "sp-uf69vpvofpmmj89st4ds"
  host_name                           = "lvcnlnx6056q"
  vswitch_id                          = module.vswitch_prd_zg.vswitch_id
  resource_group_id                   = "rg-aek2lmtbti4wjxa"
  tags_lbnref                         = "73154"
  tags_brand                          = "LVM"
  tags_application                    = "FE"
  tags_environment                    = "Production"
  tags_project                        = "CSL"
}

module "prd_ecs02" {
  source = "./modules/ecs"
  
  data_alicloud_images_name_regex     = local.local_image_name_regex
  instance_type                       = local.prp_local_instance_type
  security_groups                     = [alicloud_security_group.prd_nsg.1.id]
  availability_zone                   = local.local_availability_zone
  instance_name                       = "lvcnlnx6059p"
  system_disk_category                = "cloud_essd"
  system_disk_auto_snapshot_policy_id = "sp-uf69vpvofpmmj89st4ds"
  host_name                           = "lvcnlnx6057q"
  vswitch_id                          = module.vswitch_prd_zg.vswitch_id
  resource_group_id                   = "rg-aek2lmtbti4wjxa"
  tags_lbnref                         = "73155"
  tags_brand                          = "LVM"
  tags_application                    = "BO"
  tags_environment                    = "Production"
  tags_project                        = "CSL"
}

module "prd_mysql01" {
  source = "./modules/mysql"

  db_instance_type         = local.prd_local_mysql_type
  db_instance_storage_type = "local_ssd"
  db_engine_version        = "8.0"
  db_instance_storage      = 40
  db_instance_name         = "lv-mysql-prd-csl"
  db_resource_group_id     = "rg-aek2lmtbti4wjxa"
  db_zone_id               = local.local_availability_zone
  db_vswitch_id            = module.vswitch_prd_zg.vswitch_id
  db_security_group_ids    = alicloud_security_group.prd_nsg.*.id
  db_tag_project           = "CSL"
  db_tag_environment       = "Production"
}
resource "alicloud_kvstore_instance" "kvstore" {
    instance_name     = var.kvstore_instance_name
    instance_class    = var.kvstore_instance_class
    availability_zone = var.kvstore_instance_availability_zone
    instance_type     = var.kvstore_instance_type
    vswitch_id        = var.kvstore_vswitch_id
    engine_version    = var.kvstore_engine_version
    security_group_id = var.kvstore_security_group_id
    resource_group_id = var.kvstore_resource_group_id
    tags = {
      Project     = var.kvstore_tag_project
      Environment = var.kvstore_tag_environment
    }
}

resource "random_password" "password" {
  length           = 16
  special          = true
  override_special = "_%@"
}

resource "alicloud_kvstore_account" "account" {
  instance_id      = alicloud_kvstore_instance.kvstore.id
  account_name     = var.kvstore_account_name == "" ? lower(alicloud_kvstore_instance.kvstore.instance_name) : var.kvstore_account_name
  account_password = random_password.password.result

  lifecycle {
    ignore_changes = [
      account_password,
    ]
  }
}

resource "alicloud_kvstore_backup_policy" "default" {
  instance_id   = alicloud_kvstore_instance.kvstore.id
  backup_period = var.kvstore_backup_period
  backup_time   = var.kvstore_backup_time
}
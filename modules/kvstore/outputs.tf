output "kvstore_random_password" {
    value = random_password.password.result
}

output "redis_user" {
    value = alicloud_kvstore_account.account.account_name
}
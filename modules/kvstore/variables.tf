variable "kvstore_instance_name" {
    type        = string
    description = "The name of DB instance. It a string of 2 to 256 characters"
}

variable "kvstore_instance_class" {
    type        = string
    description = "(Required) Type of the applied ApsaraDB for Redis instance. It can be retrieved by data source alicloud_kvstore_instance_classes or referring to help-docs Instance type table"
}

variable "kvstore_instance_availability_zone" {
    type        = string
    default     = ""
    description = "(optional) describe your variable"
}

variable "kvstore_instance_type" {
    type        = string
    default     = "Redis"
    description = "The engine to use: Redis or Memcache. Defaults to Redis"
}

variable "kvstore_vswitch_id" {
    type        = string
    description = "The ID of VSwitch"
}

variable "kvstore_engine_version" {
    type        = string
    default     = "5.0"
    description = "Engine version. Supported values: 2.8, 4.0 and 5.0."
}

variable "kvstore_security_group_id" {
    type        = string
    description = "The Security Group ID of ECS"
}

variable "kvstore_resource_group_id" {
    type        = string
    default     = ""
    description = "The ID of resource group which the resource belongs"
}

variable "kvstore_tag_project" {
    type = string
    description = "project name string in tag"
}

variable "kvstore_tag_environment" {
    type = string
    description = "environment string in tag"
}

variable "kvstore_account_name" {
    type        = string
    default     = ""
    description = "Operation account requiring a uniqueness check. It may consist of lower case letters, numbers, and underlines, and must start with a letter and have no more than 16 characters"
}

variable "kvstore_backup_period" {
    type        = list
    default     = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
    description = "Backup Cycle. Allowed values: Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday"
}

variable "kvstore_backup_time" {
    type        = string
    default     = "03:00Z-04:00Z"
    description = "Backup time, in the format of HH:mmZ- HH:mmZ"
}
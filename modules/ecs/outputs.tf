output "ecs_password" {
    value = random_password.password.result
}

output "id" {
    value = alicloud_instance.instance.id
}
variable "data_alicloud_images_owners" {
    type        = string
    default     = "system"
    description = "possible value: system, self, others, marketplace"
}

variable "data_alicloud_images_name_regex" {
    type        = string
    default     = "^centos_7_8_x64"
    description = "images regex name like '^centos_6', '^centos_7_8_x64'"
}

variable "instance_type" {
    type        = string
    default     = "ecs.ic5.large"
    description = "instance type like ecs.ic5.large, ecs.c6e.large ..."
}

variable "security_groups" {
    type        = list
    default     = []
    description = "security groups"
}

variable "availability_zone" {
    type        = string
    default     = "cn-shanghai-g"
    description = "availability zone"
}

variable "instance_name" {
    type        = string
    default     = ""
    description = "instance name"
}

variable "system_disk_category" {
    type        = string
    default     = "cloud_ssd"
    description = "possible value: ephemeral_ssd, cloud_efficiency, cloud_ssd, cloud_essd, cloud. cloud only is used to some none I/O optimized instance. Default to cloud_efficiency"
}

variable "system_disk_size" {
    type        = number
    default     = 40
    description = "Size of the system disk, measured in GiB. Value range: [20, 500]"
}

variable "system_disk_auto_snapshot_policy_id" {
    type = string
    description = "The ID of the automatic snapshot policy applied to the system disk"
}

variable "internet_charge_type" {
    type        = string
    default     = "PayByTraffic"
    description = "possible value: PayByBandwidth, PayByTraffic"
}

variable "host_name" {
    type        = string
    default     = ""
    description = "host name of the ECS"
}

variable "vswitch_id" {
    type        = string
    description = "The virtual switch ID to launch in VPC"
}

variable "instance_charge_type" {
    type        = string
    default     = "PostPaid"
    description = "possible value: PrePaid, PostPaid"
}

variable "resource_group_id" {
    type        = string
    default     = ""
    description = "The Id of resource group which the instance belongs"
}

variable "data_disk_name" {
    type        = string
    default     = ""
    description = "Configuration of the disks to attach to the ECS instance: type and size"
}

variable "data_disk_size" {
    type        = number
    default     = 20
    description = "possible values: cloud：[5, 2000], cloud_efficiency：[20, 32768], cloud_ssd：[20, 32768], cloud_essd：[20, 32768], ephemeral_ssd: [5, 800]"
}

variable "data_disk_category" {
    type        = string
    default     = "cloud_ssd"
    description = "possible values: cloud, cloud_efficiency, cloud_ssd, cloud_essd, ephemeral_ssd"
}

variable "data_disk_description" {
    type        = string
    default     = "DataDisk"
    description = "The description of the data disk"
}

variable "data_disk_encrypted" {
    type        = bool
    default     = false
    description = "true or false"
}

variable "data_disk_kms_key_id" {
    type        = string
    default     = ""
    description = "The KMS key ID corresponding to the Nth data disk"
}

variable "data_disk_auto_snapshot_policy_id" {
    type        = string
    default     = ""
    description = "The ID of the automatic snapshot policy applied to the system disk"
}

variable "tags_lbnref" {
    type    = string
    default = ""
}

variable "tags_created" {
    type    = string
    default = "Terraform"
}

variable "tags_brand" {
    type = string
}

variable "tags_application" {
    type = string
}

variable "tags_environment" {
    type = string
}

variable "tags_project" {
    type = string
}

variable "private_ip" {
    type        = string
    default     = ""
    description = "Instance private IP address can be specified when you creating new instance. It is valid when vswitch_id is specified. When it is changed, the instance will reboot to make the change take effect"
}
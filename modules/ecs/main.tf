resource "random_password" "password" {
    length           = 16
    min_special      = 1
    special          = true
    override_special = "_%@"
}

resource "alicloud_instance" "instance" {
    image_id                            = data.alicloud_images.my_image.images.0.id
    instance_type                       = var.instance_type
    security_groups                     = var.security_groups
    availability_zone                   = var.availability_zone
    instance_name                       = var.instance_name
    system_disk_category                = var.system_disk_category
    system_disk_size                    = var.system_disk_size
    system_disk_auto_snapshot_policy_id = var.system_disk_auto_snapshot_policy_id
    internet_charge_type                = var.internet_charge_type
    host_name                           = var.host_name
    password                            = random_password.password.result
    vswitch_id                          = var.vswitch_id
    instance_charge_type                = var.instance_charge_type
    resource_group_id                   = var.resource_group_id

    # data_disks {
    #     name                    = var.data_disk_name
    #     size                    = var.data_disk_size
    #     category                = var.data_disk_category
    #     description             = var.data_disk_description
    #     encrypted               = var.data_disk_encrypted
    #     kms_key_id              = var.data_disk_kms_key_id
    #     auto_snapshot_policy_id = var.data_disk_auto_snapshot_policy_id 
    # }

    tags = {
        LBNREF      = var.tags_lbnref,
        Created     = var.tags_created,
        Brand       = var.tags_brand,
        Application = var.tags_application,
        Environment = var.tags_environment,
        Project     = var.tags_project
    }

    private_ip = var.private_ip
}
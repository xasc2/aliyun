data "alicloud_images" "my_image" {
    owners     = var.data_alicloud_images_owners
    name_regex = var.data_alicloud_images_name_regex
}
variable "vswitch_availability_zone" {
    type        = string
    default     = ""
    description = "The AZ for the switch"
}

variable "vswitch_vpc_id" {
    type        = string
    default     = ""
    description = "The VPC ID"
}

variable "vswitch_cidr_block" {
    type        = string
    default     = ""
    description = "The CIDR block for the switch"
}

variable "vswitch_name" {
    type        = string
    default     = ""
    description = "(optional) describe your variable"
}

variable "vswitch_tag_project" {
    type        = string
    default     = ""
    description = "(optional) describe your variable"
}

variable "vswitch_tag_env" {
    type        = string
    default     = ""
    description = "(optional) describe your variable"
}
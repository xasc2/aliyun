resource "alicloud_vswitch" "vsw" {
  availability_zone = var.vswitch_availability_zone
  vpc_id            = var.vswitch_vpc_id
  cidr_block        = var.vswitch_cidr_block
  name              = var.vswitch_name

  tags = {
    Project = var.vswitch_tag_project
    Environment = var.vswitch_tag_env
  }
}
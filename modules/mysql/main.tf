resource "alicloud_db_instance" "db" {
    engine                   = var.db_engine
    engine_version           = var.db_engine_version
    instance_type            = var.db_instance_type
    instance_storage         = var.db_instance_storage
    db_instance_storage_type = var.db_instance_storage_type
    instance_name            = var.db_instance_name
    instance_charge_type     = var.db_instance_charge_type
    resource_group_id        = var.db_resource_group_id
    zone_id                  = var.db_zone_id
    vswitch_id               = var.db_vswitch_id
    security_group_ids       = var.db_security_group_ids
    tags = {
        Project     = var.db_tag_project
        Environment = var.db_tag_environment
    }
}
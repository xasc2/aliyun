variable "db_engine" {
    type        = string
    default     = "MySQL"
    description = "Database type. Value options: MySQL, SQLServer, PostgreSQL, and PPAS"
}

variable "db_engine_version" {
    type        = string
    default     = "5.7"
    description = "Database version. Value options can refer to the latest docs CreateDBInstance"
}

variable "db_instance_type" {
    type        = string
    default     = "rds.mysql.t1.small"
    description = "DB Instance type. For details, see Instance type table: https://www.alibabacloud.com/help/zh/doc-detail/26312.htm"
}

variable "db_instance_storage" {
    type        = number
    default     = 50
    description = "User-defined DB instance storage space. Value range:[5, 2000] for MySQL/PostgreSQL/PPAS HA dual node edition;[20,1000] for MySQL 5.7 basic single node edition;[10, 2000] for SQL Server 2008R2;[20,2000] for SQL Server 2012 basic single node edition Increase progressively at a rate of 5 GB. For details, see Instance type table. Note: There is extra 5 GB storage for SQL Server Instance and it is not in specified"
}

variable "db_instance_storage_type" {
    type        = string
    default     = "local_ssd"
    description = "The storage type of the instance. Valid values: local_ssd: specifies to use local SSDs. This value is recommended. cloud_ssd: specifies to use standard SSDs. cloud_essd: specifies to use enhanced SSDs (ESSDs). cloud_essd2: specifies to use enhanced SSDs (ESSDs). cloud_essd3: specifies to use enhanced SSDs (ESSDs)."
}

variable "db_instance_name" {
    type        = string
    default     = ""
    description = "The name of DB instance. It a string of 2 to 256 characters"
}

variable "db_instance_charge_type" {
    type        = string
    default     = "Postpaid"
    description = "Valid values are Prepaid, Postpaid, Default to Postpaid. Currently, the resource only supports PostPaid to PrePaid"
}

variable "db_resource_group_id" {
    type        = string
    description = "The ID of resource group which the DB instance belongs"
}

variable "db_zone_id" {
    type        = string
    description = "The Zone to launch the DB instance. From version 1.8.1, it supports multiple zone. If it is a multi-zone and vswitch_id is specified, the vswitch must in the one of them. The multiple zone ID can be retrieved by setting multi to 'true' in the data source "
}

variable "db_vswitch_id" {
    type        = string
    description = "The virtual switch ID to launch DB instances in one VPC. If there are multiple vswitches, separate them with commas"
}

variable "db_security_group_ids" {
    type        = list
    description = "The list IDs to join ECS Security Group. At most supports three security groups"
}

variable "db_tag_project" {
    type        = string
    description = "project name string in tags"
}

variable "db_tag_environment" {
    type        = string
    description = "environment string in tags"
}
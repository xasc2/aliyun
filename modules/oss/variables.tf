variable "bucket_name" {
    type        = string
    default     = ""
    description = "The name of the bucket. If omitted, Terraform will assign a random and unique name"
}

variable "bucket_acl" {
    type        = string
    default     = "private"
    description = "The canned ACL to apply. Can be 'private', 'public-read' and 'public-read-write'. Defaults to 'private'"
}

variable "oss_lifecycle_rule_id" {
    type        = string
    default     = ""
    description = "Unique identifier for the rule. If omitted, OSS bucket will assign a unique name"
}

variable "oss_lifecycle_rule_prefix" {
    type        = string
    default     = "null"
    description = "Object key prefix identifying one or more objects to which the rule applies. Default value is null, the rule applies to all objects in a bucket"
}

variable "oss_lifecycle_rule_enabled" {
    type        = bool
    default     = false
    description = "Specifies lifecycle rule status"
}

variable "oss_lifecycle_rule_expiration_days" {
    type        = number
    default     = 365
    description = "Specifies the number of days after object creation when the specific rule action takes effect"
}

variable "oss_tag_project" {
    type        = string
    default     = ""
    description = "project tag"
}

variable "oss_tag_environment" {
    type        = string
    default     = ""
    description = "environment tag"
}
resource "alicloud_oss_bucket" "bucket" {
  bucket = var.bucket_name
  acl    = var.bucket_acl

  lifecycle_rule {
    id      = var.oss_lifecycle_rule_id
    prefix  = var.oss_lifecycle_rule_prefix
    enabled = var.oss_lifecycle_rule_enabled

    expiration {
      days = var.oss_lifecycle_rule_expiration_days
    }
  }

  tags = {
    Project     = var.oss_tag_project
    Environment = var.oss_tag_environment
  }
}
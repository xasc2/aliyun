variable "kms_alias_name" {
    type = string
    description = "The alias of CMK. Encrypt、GenerateDataKey、DescribeKey can be called using aliases. Length of characters other than prefixes: minimum length of 1 character and maximum length of 255 characters. Must contain prefix alias/"
}
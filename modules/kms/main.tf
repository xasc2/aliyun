resource "alicloud_kms_key" "kms_key" {

}

resource "alicloud_kms_alias" "kms_key_alias" {
  alias_name = var.kms_alias_name
  key_id     = alicloud_kms_key.kms_key.id
}

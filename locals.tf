# common values
locals {

    prd_vpc_id = "vpc-uf65nhyv3b2so2aldmn77"
    prp_vpc_id = "vpc-uf65hrrp6mclebz7kjou4"

    #vSwitch CIDRs
    prd_vsw_zf_CIDR = "10.9.33.32/28"
    prd_vsw_zg_CIDR = "10.9.33.48/28"
    prp_vsw_zf_CIDR = "10.9.35.32/28"
    prp_vsw_zg_CIDR = "10.9.35.48/28"

    #nsg names
    prd_nsg_names = ["lv-nsg-prd-csl-fe","lv-nsg-prd-csl-be"]
    prp_nsg_names = ["lv-nsg-prp-csl-fe","lv-nsg-prp-csl-be"]  

    local_image_name_regex    = "^centos_8_1_x64"
    local_availability_zone   = "cn-shanghai-g"
    prp_local_instance_type   = "ecs.c6e.large"
    prd_local_instance_type   = "ecs.hfg6.xlarge"
    prp_local_mysql_type      = "mysql.n2.small.1"
    prd_local_mysql_type      = "rds.mysql.s2.large"
    prp_local_kms_alias_name  = "alias/lv-kms-prp-csl"
    prd_local_kms_alias_name  = "alias/lv-kms-prd-csl"
}
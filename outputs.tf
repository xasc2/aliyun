output "prp_ecs01_password" {
    value = module.prp_ecs01.ecs_password
}

output "prp_ecs02_password" {
    value = module.prp_ecs01.ecs_password
}

output "prd_ecs01_password" {
    value = module.prd_ecs01.ecs_password
}

output "prd_ecs02_password" {
    value = module.prd_ecs01.ecs_password
}